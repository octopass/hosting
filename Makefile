WORDPRESS_IMAGE := octopass-wordpress

.DEFAULT_GOAL := help
.PHONY: wordpress

# https://blog.thapaliya.com/posts/well-documented-makefiles/
help:  ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  $(MAKE) \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

wordpress:
	docker build wordpress --tag=$(WORDPRESS_IMAGE)