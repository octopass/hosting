# OctoPass Hosting

An opinionated way of hosting websites using Docker with [CapRover](https://caprover.com/).

Assumptions:

- This is not for reselling, but for hosting our own websites.
- We use [CloudFlare](https://www.cloudflare.com/) for DNS and TLS.
- We don't host e-mail ourselves. Use a managed solution like [Zoho Mail](https://www.zoho.com/mail/).

## New Server Setup

1. Create a Debian 10 VPS on your provider.
2. Point a DNS name to this new server. This name will be asked during the setup and you'll need it to access all the administrative tools.
3. Connect to your new server via SSH as root (e.g. `ssh root@myserver.com`).
4. At the command prompt, execute the commands below, one by one.

```bash
apt update && apt install -y git
git clone https://gitlab.com/octopass/hosting.git /hosting
cd /hosting
./setup
```

If (hopefully) nothing outputted an error, your new server is ready. Now it's time to install CapRover. [Follow the tutorial](https://caprover.com/docs/get-started.html) to get it running.

## Create a WebSite

1. Go to CloudFlare and create or edit the domain for the new website. Create an A record pointing to the IP address of the hosting server. Don't proxy the domain yet.
2. On your web browser, go to `https://myserver.com` (where `myserver.com` is the hostname of your hosting server). Use your administrator password to log in.
3. On the left menu, click on **Apps**, then on **One-Click Apps/Databases**.
4. From the apps list, select **WordPress** and fill in the form.
5. Click on **Deploy**, wait a minute, and your website will be alive.

### Uploading Files

Use an SFTP client like FileZilla with the following connection parameters:

- Host: `myserver.com` (replace with the real name)
- User: `root`
- Password: (your root password)
- Directory: `/var/lib/docker/volumes/captain--APP_NAME-wp-data/_data` (where `APP_NAME` is the name you gave to the application on creation)

### PHPMyAdmin

On your web browser, go to `https://phpmyadmin.myserver.com` (where `myserver.com` is the hostname of your new server). You can safely ignore any certificate warning here.

The server name for any database is `srv-captain--APP_NAME-db`, where `APP_NAME` is the name you gave when created the application.

### SMTP Server

If you app sends e-mails, you can use this:

- Host: `hosting_smtp`
- User: (leave it empty)
- Password: (leave it empty)

Your e-mail sender must have the hostname of your hosting server as domain. For example, if your hosting server is `myserver.com`, a valid sender will be `no-reply@myserver.com`.
