# This is a PHP image containing everything we use at OctoPass.
# Feel free to add more if necessary (but be sure you don't add anything not really needed).

FROM wordpress:5.3.2

RUN apt-get update && \
	apt-get install --yes \
	# Needed by the GD PHP extension
	libfreetype6-dev \
	libjpeg62-turbo-dev \
	libpng-dev \
	# ---
	libmcrypt-dev \
	libzip-dev \
	# ---
	default-mysql-client \
	mysql-common \
	# Not necessary to run, but can be useful for quick fixes and workarounds.
	nano \
	# Cleanup
	&& apt-get clean

# PHP Extensions
RUN ["docker-php-ext-install", "bcmath"]
# RUN ["docker-php-ext-install", "mcrypt"]
RUN ["docker-php-ext-install", "mysqli"]
RUN ["docker-php-ext-install", "pdo_mysql"]
# RUN ["docker-php-ext-install", "zip"]

# GD requires some more work
RUN docker-php-ext-configure gd \
	--with-freetype-dir=/usr/include/ \
	--with-jpeg-dir=/usr/include/ \
	&& docker-php-ext-install -j$(nproc) gd

# Enable popular Apache modules
RUN ["a2enmod", "deflate"]
RUN ["a2enmod", "filter"]
RUN ["a2enmod", "headers"]
RUN ["a2enmod", "rewrite"]
RUN ["a2enmod", "setenvif"]
RUN ["a2enmod", "ssl"]